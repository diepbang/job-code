#!/usr/bin/env python
# -*- coding: utf-8 -*- 
from __future__ import unicode_literals
import spacy
# nlp = spacy.load('en')
import magic
mime = magic.Magic(mime=True)
##
import os
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.svm import LinearSVC
from sklearn import preprocessing
from sklearn.multiclass import OneVsOneClassifier
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.multiclass import OneVsRestClassifier
from sklearn import svm
from sklearn import linear_model
from sklearn import preprocessing
import numpy as np
from nltk.internals import find_jars_within_path
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.svm import LinearSVC
from sklearn import preprocessing
from sklearn.multiclass import OneVsOneClassifier
import re, nltk
#
from module import convertPdf
from sklearn.linear_model import LogisticRegression
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import train_test_split
from sklearn import datasets, svm
from sklearn import cross_validation
##
from module import google_nlp
from module import job_finder
from module import job_finder_uni
from sklearn.externals import joblib
#
import tornado.ioloop
import tornado.web
from tornado import httpserver
from tornado.gen import *
from tornado.web import *
import re 
import json
from tornado import httpclient
import urllib


ULOAD_TMP_FOLDER = "/home/diepbang/upload_tmp/"
# content = convertPdf.convert("resume.pdf")
# content = re.sub(r'[^\x00-\x7f]',r' ', content)
# content = content.split("\n")
##
##
##
job_finder = job_finder.finder()
job_finder.loadData()
classifier = job_finder.initClassifier()
##
# joblib.dump(classifier, 'pickle/skill_finder.pkl')
##
##
# skill = ["ruby",'rails','sql','excel']
# length_skill = len(skill)
def cosine_sim(text1, text2):
    try:
        vectorizer = TfidfVectorizer(lowercase=True)
        tfidf = vectorizer.fit_transform([text1, text2])
        return ((tfidf * tfidf.T).A)[0,1]
    except:
        return 0
    #
def checkIf_found(text,skill):
    r = None
    for item_skill in skill:
        if text == item_skill:
            r = item_skill
            break
        else:
            if cosine_sim(text,item_skill) > 0:
                r = item_skill
                break
    return r
###
# total_found = length_skill - len(skill)
# percent = (total_found * 100) / length_skill
####
####
# print(str(percent) + " % ")
# print("percent")
# print(cosine_sim("c#","c#"))





class analyzerClass(tornado.web.RequestHandler):
    def get(self):
        self.render("demo_static/demo_job.html")
    def post(self):
        # print(self.request.files['filearg'])
        skill_require = self.get_argument("skill")
        fileinfo = self.request.files['filearg'][0]
        file_name = fileinfo['filename']
        # print "fileinfo is", fileinfo
        save_path = ULOAD_TMP_FOLDER+file_name
        fh = open(save_path,'wb')
        fh.write(fileinfo['body'])
        fh.close()
        #application/pdf
        if mime.from_file(save_path) == "application/pdf":
            ##
            content = convertPdf.convert(save_path)
            content = re.sub(r'[^\x00-\x7f]',r' ', content)
            content = content.replace("(","")
            content = content.replace(")","")
            content = content.replace(",","")
            content = content.replace(".","")
            content = content.split("\n")
            #
            # content = content.replace("\n"," . ")
            # print(text)
            # content = google_nlp.tag(content)
            # content = " ".join(content)
            # #
            # content = content.split("__")
            # content = "\n".join(content)
            # content = content.split("\n")
            #
            # print("newn content")
            # print(content)
            # print("#####")
            # content = google_nlp.tag(content)
            # content = "\n".join(content)
            # print(content)
            # print("aftercon")
            print(content)
            ##
            skill = skill_require.split(",")
            first_length = len(skill)
            #
            X_test = np.array(content)
            predicted = classifier.predict(X_test)
            index = 0
            ##
            result_output = ""
            # print(predicted)
            for item in predicted:
                if item in ["TECH SKILL","LANGUAGE"]:
                    key_item = content[index].lower()
                    #####
                    found_el = checkIf_found(key_item,skill)
                    #
                    if found_el is not None:
                        skill.remove(found_el)
                    result_output += content[index] + "\n"
                    print(content[index] + " --- > "+item)
                index = index + 1
            ##
            result_output = result_output.split(" ")
            X_test = np.array(result_output)
            predicted = classifier.predict(X_test)
            index = 0
            #
            for item in predicted:
                if item in ["TECH SKILL","LANGUAGE"]:
                    key_item = result_output[index].lower()
                    #####
                    found_el = checkIf_found(key_item,skill)
                    #
                    if found_el is not None:
                        skill.remove(found_el)
                    result_output += result_output[index] + "\n"
                    print(result_output[index] + " --- > "+item)
                index = index + 1
            #
            # for item_content in content:
            #     if len(item_content.strip()) > 0:
            #         # content_tag = google_nlp.tag(item)
            #         to_ar = [item_content]
            #         #
            #         X_test = np.array(to_ar)
            #         predicted = classifier.predict(X_test)
            #         #
            #         index = 0
            #         for item in predicted:
            #             if item in ["TECH SKILL","LANGUAGE"]:
            #                 key_item = to_ar[index].lower()
            #                 found_el = checkIf_found(key_item,skill)
            #                 ###
            #                 ###
            #                 if found_el is not None:
            #                     print(found_el)
            #                     skill.remove(found_el)
                                
            #                 print(to_ar[index] + " --- > "+item)
            #             index = index + 1
            #         ####
            print(skill)
            total_found = first_length - len(skill)
            percent = (total_found * 100) / first_length
            self.write(str(percent) + " % ")
        else:
            os.remove(save_path)
            self.write("done")
        # self.finish("ok")

if __name__ == "__main__":
    application = tornado.web.Application([
        (r"/analyzer",analyzerClass)
    ])
    server = tornado.httpserver.HTTPServer(application)
    server.bind(9867)
    server.start(0)
    ##
    tornado.ioloop.IOLoop.current().start()

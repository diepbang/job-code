#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import pandas
import datetime as dt
from datetime import datetime
#
import tornado.ioloop
import tornado.web
import requests
from tornado import httpserver
from tornado.gen import *
from tornado.web import *
import re
#
from collections import Counter
from pprint import pprint
import json
from tornado import httpclient
import urllib
from datetime import datetime
import parsedatetime as pdt
#
import numpy


# print(pandas.date_range("2016-11-11", "2016-12-11", freq="30min",tz="Asia/Singapore"))
print("New version 10.5");
def get_time_list(start,end,exception_list,timezone,email):
    if timezone is None:
        timezone = "Asia/Singapore"
    #
    list_date_time = pandas.date_range(start,end, freq="30min",tz=timezone)   
    hour_exception = [0,23,24,21,22,1,2,3,4,5,6,7]
    busy_date_time = []
    busy_date_time = exception_list
    #
    
    previous_date = None
    previous_month = None
    previous_item = None
    #
    sum_list = []
    new_chapter = 0
    print("#exception list")
    print(exception_list)
    print(busy_date_time)
    print("####exception list")
    # index = 0
    for item in list_date_time:
        item_str = str(item)
        if item_str not in busy_date_time:
            if email is not None:
                r = requests.get('http://ggvlabs.com:8997/check_freetime/week?email='+email+'&date='+item_str)
                r_js = json.loads(r.text)
                # print(r.text)
                # print(r_js['status'])
                #
                #
                if r_js['status'] == "free":
                    if item.hour not in hour_exception:
                        # print(item_str)
                        if previous_item is not None:
                            item_cur = numpy.datetime64(item).astype(datetime)
                            previous_cur = numpy.datetime64(previous_item).astype(datetime)
                            total_left = 0
                            total_left = (item_cur - previous_cur).total_seconds()
                            total_left = int(total_left)
                            ##
                        if item.date() == previous_date and item.month == previous_month and total_left == 1800:
                            sum_list[new_chapter].append(item_str)
                        else:
                            # print("====")
                            if len(sum_list) <= 0:
                                sum_list.append([])
                                sum_list[new_chapter].append(item_str)
                            else:
                                sum_list.append([])
                                sum_list[new_chapter + 1].append(item_str)
                                new_chapter = new_chapter + 1
                        previous_item = item
                        previous_date = item.date()
                        previous_month = item.month
                    #
    print("finished all");
    # print(sum_list)
    new_item_list = []
    for item in sum_list:
        string = ""
        if len(item) > 1:
            string += item[0] + " {to} "+ item[len(item) - 1]
        else:
            string = item[0]
        new_item_list.append(string)
    print("finished all --- ")
    print(new_item_list)
    return new_item_list
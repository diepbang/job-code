
import json
import sys
import os
#export GOOGLE_APPLICATION_CREDENTIALS=/media/nlp/avary.json
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/media/nlp/avary.json'
##s
from googleapiclient import discovery
import httplib2
from oauth2client.client import GoogleCredentials


def get_native_encoding_type():
    """Returns the encoding type that matches Python's native strings."""
    if sys.maxunicode == 65535:
        return 'UTF16'
    else:
        return 'UTF32'

###
def analyze_syntax(text, encoding='UTF32'):
    body = {
        'document': {
            'type': 'PLAIN_TEXT',
            'content': text,
        },
        'features': {
            'extract_syntax': True,
        },
        'encodingType': encoding,
    }

    service = get_service()
    request = service.documents().annotateText(body=body)
    response = request.execute()
    return response

def get_service():
    credentials = GoogleCredentials.get_application_default()
    scoped_credentials = credentials.create_scoped(
        ['https://www.googleapis.com/auth/cloud-platform'])
    http = httplib2.Http()
    scoped_credentials.authorize(http)
    return discovery.build('language', 'v1beta1', http=http)
    
def tag(content):
    try:
        found_noun = 0
        print("hola")
        #  
        print(content)
        result = analyze_syntax(content, get_native_encoding_type())
        list = []
        # print
        # print(json.dumps(result,indent=4, sort_keys=True))
        for item in result['tokens']:
            # print(item['partOfSpeech']['tag'])
            # dep_label = item['dependencyEdge']['label'].lower()
            # dep_position = item['dependencyEdge']['headTokenIndex']
            # dep_position = int(dep_position)
            # print("---")
            # print(item['text']['content'])
            # print(result['tokens'][dep_position]['text']['content'])
            # if dep_label == "attr":
            #     print(item['text']['content'])
            # if dep_label == "nn":
            #     print(item['text']['content'])
            # if dep_label == "amod":
            #     print(item['text']['content'])
            # if dep_label == "pobj":
            #     print(item['text']['content'])
            # if dep_label == "root":
            #     print(item['text']['content'])
            print("----")
            if item['partOfSpeech']['tag'] == "NOUN":
                # print(item['text']['content'])
                # print(item['partOfSpeech']['tag'])
                list.append(item['text']['content'])
                found_noun = found_noun + 1
            else:
                list.append(",")
            ##
        if found_noun > 0:
            list = " ".join(list)
            list = list.split(",")
            #
            result_list = [] 
            for item in list:
                if item is not None and len(item.strip()) > 0:
                    result_list.append(item.strip())
            # print(result_list)
            return result_list
        else:
            return ['nono']
    except:
        pass
        return ["nono"]
    


#!/usr/bin/env python
# -*- coding: utf-8 -*- 
from __future__ import unicode_literals
#
from nltk.internals import find_jars_within_path
from nltk.tree import *
import nltk
import re
from sklearn.naive_bayes import MultinomialNB
from nltk.parse.stanford import StanfordParser
from sklearn.externals import joblib
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn import preprocessing
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from nltk.classify import SklearnClassifier
from sklearn.svm import SVC
from collections import Counter
from sklearn.linear_model import LogisticRegression,SGDClassifier
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from module import postagger
import os.path

from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report


class finder:
    ####
    ####
    def __init__(self):
        self.Y_label = []
        self.Y = None
        self.total_data_list = []
    def loadData(self):
        #
        self.y_train_text = []
        self.y_train_text_2 = []
        #
        job_total = ""
        for path in ['/media/nlp/data2/tech.txt','/media/nlp/data2/job_keyword.txt','/media/nlp/data2/job_extras.txt']:     
            name_data = open(path, 'r') 
            print(path)
            job_total +=   "\n" +  re.sub(r'[^\x00-\x7f]',r'', name_data.read())
            name_data.close()
        job_total = job_total.split("\n")
        ##
        ##
        lang_total = ""
        for path in ['/media/nlp/data2/lang.txt']:     
            name_data = open(path, 'r') 
            print(path)
            lang_total +=   "\n" +  re.sub(r'[^\x00-\x7f]',r'', name_data.read())
            name_data.close()
        lang_total = lang_total.split("\n")
        ###
        ##
        ##
        ##
        print(len(job_total))
        common_list = ""
        for path in ['/media/nlp/data/common_2.txt'
        ,'/media/nlp/data/location/country.txt'
        ,'/media/nlp/data/location/continent.txt']:
            common_data = open(path, 'r') 
            # print(path)
            common_list +=  "\n" +  re.sub(r'[^\x00-\x7f]',r'', common_data.read())
            common_data.close()

        common_list = common_list.split("\n")
        ##trandiepbang_thaikhiet
        # print(len(name_list))
        print("#common list")
        print(len(common_list)) 
        # common_list = common_list[:1000]
        self.total_data_list = job_total + lang_total  + common_list
        print("TOTAL DATA LEARN")
        print(len(self.total_data_list))
        #
        self.label = ["TECH SKILL","LANGUAGE","common"]
        self.index = 0
        for item_array in [job_total,lang_total,common_list]:
            for item_array_2 in item_array:
                # self.y_train_text.append([self.label[self.index]])
                self.y_train_text_2.append(self.label[self.index])
            self.index = self.index + 1
        ##
        ##
        self.X_train = np.array(self.total_data_list)
        # print(self.y_train_text)
        print("x train")
        ##
        # self.Y_label = self.y_train_text
        # self.lb = preprocessing.MultiLabelBinarizer()
        # self.Y = self.lb.fit_transform(self.y_train_text)
        
    def get_lb(self):
        return self.lb
    
    def initClassifier(self):
        # self.count_vect = CountVectorizer(analyzer='char_wb',stop_words = 'english',lowercase = True,
        #                                     ngram_range=(1,9), min_df=1)
        # X_train_counts = self.count_vect.fit_transform(self.total_data_list)
        # #
        # self.tfidf_transformer = TfidfTransformer()
        # X_train_tfidf = self.tfidf_transformer.fit_transform(X_train_counts)
        # scores = ['precision', 'recall']
        # tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
        #              'C': [1, 10,20,30,40,50,60,70,80,90,100,200,300,500,600,650,700,750,1000,1600,1700,1500,1300,1200,1100,1005]},
        #             {'kernel': ['linear'], 'C': [1, 10,20,23,24,25,30,33,34,36,35,37,38,40,50,60,70,80,90,100,200,300,500,600,650,700,1000,1600,1700,1500,1300,1200,1100,1005]}]
        # ##
        # ##
        # X_train, X_test, y_train, y_test = train_test_split(
        #             X_train_tfidf, 
        #             self.y_train_text_2,test_size=0.5, random_state=0
        # )
        # print("# Tuning test for job_finer education ##")
        # for score in scores:
        #     print("# Tuning hyper-parameters for %s" % score)
        #     print()

        #     clf = GridSearchCV(SVC(C=1), tuned_parameters, cv=5,
        #                scoring='%s_weighted' % score)
        #     clf.fit(X_train, y_train)

        #     print("Best parameters set found on development set:")
        #     print()
        #     print(clf.best_params_)
        #     print()
        #     print("Grid scores on development set:")
        #     print()
        #     print()

        #     print("Detailed classification report:")
        #     print()
        #     print("The model is trained on the full development set.")
        #     print("The scores are computed on the full evaluation set.")
        #     print()
        #     y_true, y_pred = y_test, clf.predict(X_test)
        #     print()
        classifier = Pipeline([
                                ('vectorizer', CountVectorizer(
                                    analyzer='char_wb',
                                    stop_words = 'english',
                                    lowercase = True,ngram_range=(1,9))
                                ),
                                ('tfidf', TfidfTransformer()),
                                ('clf', SVC(C=500,kernel='rbf',gamma=0.001))])
        classifier.fit(self.X_train,self.y_train_text_2)
        
        # if os.path.isfile('pickle/skill_finder.pkl'):
        #     print("file exist - skill_finder")
        #     classifier = joblib.load('pickle/skill_finder.pkl')
        # else:
            
        ####
        return classifier
    
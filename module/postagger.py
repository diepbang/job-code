from __future__ import unicode_literals
import spacy
####
from nltk.internals import find_jars_within_path
from nltk.tree import *
import nltk 
import re
from nltk.parse.stanford import StanfordParser
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn import preprocessing
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from nltk.classify import SklearnClassifier
from collections import Counter
from sklearn.linear_model import LogisticRegression,SGDClassifier
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

class posTagger:
    def __init__(self,nlp):
        self.nlp = nlp
    def tag(self,text):
        text = re.sub(r'[^\x00-\x7f]',r'', text)
        pos_tag_list = []
        ##
        parsed = self.nlp(text)
        for sent in parsed:
            pos_tag_list.append(sent.pos_)
        # print(pos_tag_list)
        return pos_tag_list
    def tag_with_label(self,text):
        doc = self.nlp(text)
        pos_lits = []
        for sent in doc:
            text = str(sent)
            pos_lits.append((text,sent.pos_))
        return pos_lits
    ###
    ##
    def tag_muti(self,text_ar):
        if len(text_ar) > 0:
            tag_ar = []
            for item_text in text_ar:
                text_speech = self.tag(item_text)
                text_speech = " ".join(text_speech)
                tag_ar.append(text_speech)
            return tag_ar
        else:
            return []
        # text = re.sub(r'[^\x00-\x7f]',r'', text)
        # pos_tag_list = []
        # result = list(self.parser.raw_parse(text))[0]
        # # print(result)
        # for pos_tag in result.subtrees():
        #     pos_tag_list.append(pos_tag.label())
        #     # print(pos_tag_list)
        # return pos_tag_list